package com.example.doshanbebazar.models;

import com.google.gson.annotations.SerializedName;

public class NewProductResponse {
    int status ;
    String message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @SerializedName("data")
    Product product;
}
