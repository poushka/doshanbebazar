package com.example.doshanbebazar.models;

import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    String message ;
    int status ;

    @SerializedName("data")
    User user;
}
