package com.example.doshanbebazar.main_activity.category_fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.doshanbebazar.R;
import com.example.doshanbebazar.api.ApiInterface;
import com.example.doshanbebazar.api.MyRetrofit;
import com.example.doshanbebazar.main_activity.category_fragment.inside_category.CategoryInsideFragment;
import com.example.doshanbebazar.models.Category;
import com.example.doshanbebazar.models.CategoryResponse;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment implements AdapterCategory.CategoryListner {

    AdapterCategory adapter ;

    ArrayList<Category> categories = new ArrayList<>() ;

    RecyclerView rv_categories ;

    ProgressBar progressBar ;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapter=new AdapterCategory(requireContext(),categories,this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_category, container, false);
        rv_categories=view.findViewById(R.id.rv_categories);
        progressBar=view.findViewById(R.id.progress_categories);
        return view ;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        rv_categories.setAdapter(adapter);
        ApiInterface apiInterface = MyRetrofit.getRetrofit().create(ApiInterface.class);
        Call<CategoryResponse> call = apiInterface.getCategories();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                progressBar.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    categories.addAll(response.body().getCategories());
                    adapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(requireContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void cat_clicked(Integer position) {
      int cat_id = categories.get(position).getCat_id();
        CategoryInsideFragment fragment = CategoryInsideFragment.newInstance(cat_id);
        getChildFragmentManager().beginTransaction().replace(R.id.container_category,fragment).addToBackStack(null).commit();


    }
}
