package com.example.doshanbebazar.main_activity.category_fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.doshanbebazar.R;
import com.example.doshanbebazar.models.Category;
import com.example.doshanbebazar.models.Product;

import java.util.ArrayList;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.Holder> {

    Context context;
    ArrayList<Category> categories;

    String pic_base_address="http://poushka.com/shop/public/images/category/";

    CategoryListner categoryListner;

    public AdapterCategory(Context context , ArrayList<Category> categories, CategoryListner categoryListner) {
        this.context=context;
        this.categories=categories;
        this.categoryListner=categoryListner;
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.custom_category,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int position) {
        holder.tv_name.setText(categories.get(position).getName());

        Glide.with(context).load(pic_base_address+categories.get(position).getPic())
                .into(holder.iv_icon);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryListner.cat_clicked(holder.getAdapterPosition());
            }
        });

    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class Holder extends RecyclerView.ViewHolder {

        ImageView iv_icon ;
        TextView tv_name ;


        public Holder(@NonNull View itemView) {
            super(itemView);
            iv_icon=itemView.findViewById(R.id.iv_category);
            tv_name=itemView.findViewById(R.id.tv_category);
        }
    }

    public interface CategoryListner {
        public void cat_clicked(Integer position);
    }

}





