package com.example.doshanbebazar.main_activity.new_product;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.doshanbebazar.R;
import com.example.doshanbebazar.api.ApiInterface;
import com.example.doshanbebazar.api.MyRetrofit;
import com.example.doshanbebazar.main_activity.category_fragment.AdapterCategory;
import com.example.doshanbebazar.models.Category;
import com.example.doshanbebazar.models.CategoryResponse;
import com.example.doshanbebazar.models.NewProductResponse;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okio.ByteString;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;


public class NewProductFragment extends Fragment implements AdapterCategory.CategoryListner, OnMapReadyCallback {


    RecyclerView rv_categories;

    AdapterCategory adapterCategory;

    ArrayList<Category> categories = new ArrayList<>();

    ProgressBar progress_cat;

    int cat_id = 0;

    ConstraintLayout cl_lvl_1, cl_lvl_2;


    ImageView iv_gallery;

    Button btn_save;

    File file = null;

    EditText et_name, et_price, et_des;

    ProgressBar pb_new;


    ApiInterface apiInterface;

    FloatingActionButton btn_location;

    Bitmap yourSelectedImage;

    FusedLocationProviderClient fusedLocationProviderClient;

    GoogleMap mMap ;


    SupportMapFragment mapFragment ;




    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adapterCategory = new AdapterCategory(requireContext(), categories, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_product, container, false);
        rv_categories = view.findViewById(R.id.rv_categories_product);
        progress_cat = view.findViewById(R.id.progress_new_product);
        cl_lvl_1 = view.findViewById(R.id.cl_level1_product);
        cl_lvl_2 = view.findViewById(R.id.cl_level2_product);
        iv_gallery = view.findViewById(R.id.iv_select_pic);
        btn_save = view.findViewById(R.id.btn_sabte_mahsul);
        et_name = view.findViewById(R.id.et_name_product);
        et_price = view.findViewById(R.id.et_price_product);
        et_des = view.findViewById(R.id.et_description_product);
        pb_new = view.findViewById(R.id.pb_new_product);
        btn_location = view.findViewById(R.id.btn_find_location);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map_new_product);

        mapFragment.getMapAsync(this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(requireActivity());



        rv_categories.setAdapter(adapterCategory);
        apiInterface = MyRetrofit.getRetrofit().create(ApiInterface.class);
        Call<CategoryResponse> call = apiInterface.getCategories();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                progress_cat.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    categories.addAll(response.body().getCategories());
                    adapterCategory.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                progress_cat.setVisibility(View.GONE);
                Toast.makeText(requireContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show();
            }
        });

        btn_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dexter.withContext(requireContext())
                        .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(new PermissionListener() {
                            @SuppressLint("MissingPermission")
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {

                                LocationManager service = (LocationManager)requireContext().getSystemService(LOCATION_SERVICE);
                                boolean enabled = service
                                        .isProviderEnabled(LocationManager.GPS_PROVIDER);
                                if (!enabled) {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivity(intent);
                                }else {
                                    fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                                        @Override
                                        public void onSuccess(Location location) {
                                            LatLng user_location = new LatLng(location.getLatitude(),location.getLongitude());
                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(user_location,14));
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                                 Toast.makeText(requireContext(),"لطفا دسترسی به موقیت جفرافیایی را تایید نمایید",Toast.LENGTH_LONG).show();

                            }
                        }).check();
            }
        });


        iv_gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dexter.withContext(requireContext())
                        .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .withListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
                                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(i, 100);
                            }

                            @Override
                            public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {

                            }
                        }).check();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = et_name.getText().toString();
                String price = et_price.getText().toString();
                String description =et_des.getText().toString();

                 LatLng finial_lat_lang =    mMap.getCameraPosition().target;

                if (file==null) {
                    Toast.makeText(requireContext(), "لطفا تصویر محصول را انتخاب نمایید", Toast.LENGTH_SHORT).show();
                }else if (name.isEmpty()) {
                    Toast.makeText(requireContext(), "لطفا نام محصول را وارد نمایید", Toast.LENGTH_SHORT).show();
                }else if (price.isEmpty()) {
                    Toast.makeText(requireContext(), "لطفا قیمت محصول را وارد نمایید", Toast.LENGTH_SHORT).show();
                }else if (description.isEmpty()) {
                    Toast.makeText(requireContext(), "لطفا توضیحات محصول را وارد نمایید", Toast.LENGTH_SHORT).show();
                }else  {


                    RequestBody req_pic =RequestBody.create(MediaType.parse("image/*"),file);
                    MultipartBody.Part mulipartBody_pic = MultipartBody.Part.createFormData("pic",file.getName(),req_pic);

                    RequestBody name_req = RequestBody.create(MediaType.parse("text/plain"), name);

                    RequestBody price_req = RequestBody.create(MediaType.parse("text/plain"),price);


                    RequestBody descriptin_Req = RequestBody.create(MediaType.parse("text/plain"),description);



                    RequestBody lat_req = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(finial_lat_lang.latitude) );

                    RequestBody lang_req = RequestBody.create(MediaType.parse("text/plain"),String.valueOf(finial_lat_lang.longitude));

                    Call<NewProductResponse> call = apiInterface.makeProduct(cat_id,mulipartBody_pic,name_req,price_req,lat_req,lang_req,descriptin_Req);

                    pb_new.setVisibility(View.VISIBLE);
                    btn_save.setText("");
                    call.enqueue(new Callback<NewProductResponse>() {
                        @Override
                        public void onResponse(Call<NewProductResponse> call, Response<NewProductResponse> response) {
                            pb_new.setVisibility(View.GONE);
                            btn_save.setText("ثبت محصول");
                            if (response.isSuccessful()) {
                                Toast.makeText(requireContext(), "ثبت با موققیت انجام شد", Toast.LENGTH_SHORT).show();
                            }
                            else {
                                Toast.makeText(requireContext(), "خطا در ثبت", Toast.LENGTH_SHORT).show();
                            }

                        }

                        @Override
                        public void onFailure(Call<NewProductResponse> call, Throwable t) {
                            Log.e("testError",t.getMessage());
                            pb_new.setVisibility(View.GONE);
                            btn_save.setText("ثبت محصول");
                            Toast.makeText(requireContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show();

                        }
                    });


                }
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==100 && resultCode== Activity.RESULT_OK) {
            Uri address =data.getData();

            iv_gallery.setImageURI(address);

            String path  = getPath(address);


            file=new File(path);
        }
    }


    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor =  requireActivity().getContentResolver().query(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(projection[0]);
        String filePath = cursor.getString(columnIndex);
        yourSelectedImage = BitmapFactory.decodeFile(filePath);
        return cursor.getString(column_index);
    }


    @Override
    public void cat_clicked(Integer position) {
         cat_id=categories.get(position).getCat_id();
         cl_lvl_1.setVisibility(View.GONE);
         cl_lvl_2.setVisibility(View.VISIBLE);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap=googleMap;
        LatLng latLng = new LatLng(38.124124,50.12421412);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,14));
    }

}