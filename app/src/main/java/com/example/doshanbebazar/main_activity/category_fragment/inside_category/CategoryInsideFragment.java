package com.example.doshanbebazar.main_activity.category_fragment.inside_category;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.doshanbebazar.R;
import com.example.doshanbebazar.api.ApiInterface;
import com.example.doshanbebazar.api.MyRetrofit;
import com.example.doshanbebazar.main_activity.main_fragment.AdapterProduct;
import com.example.doshanbebazar.models.Product;
import com.example.doshanbebazar.models.ProductResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryInsideFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryInsideFragment extends Fragment implements AdapterProduct.MahsulListener {


    public final static String CAT_ID="cat_id";

    public CategoryInsideFragment() {
        // Required empty public constructor
    }

    int cat_id = 0;

    ArrayList<Product> products = new ArrayList<>();
    AdapterProduct adapterProduct ;

    RecyclerView rv_product;

    public static CategoryInsideFragment newInstance(int cat_id) {
        CategoryInsideFragment fragment = new CategoryInsideFragment();
        Bundle args = new Bundle();
        args.putInt(CAT_ID, cat_id);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cat_id=getArguments().getInt(CAT_ID);
        }
        adapterProduct=new AdapterProduct(requireContext(),products,this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view    =  inflater.inflate(R.layout.fragment_category_inside, container, false);

        rv_product=view.findViewById(R.id.rv_product_category);

        return view ;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rv_product.setAdapter(adapterProduct);

        ApiInterface apiInterface = MyRetrofit.getRetrofit().create(ApiInterface.class);
        Call<ProductResponse> call = apiInterface.getProductsByCat(cat_id);
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()) {
                    products.addAll(response.body().getProducts());
                    adapterProduct.notifyDataSetChanged();

                }else {

                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                Toast.makeText(requireContext(), "عدم دسترسی به اینترنتا", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void mahsulClicked(Integer position) {

    }

    @Override
    public void deleteCliced(Integer position) {

    }
}