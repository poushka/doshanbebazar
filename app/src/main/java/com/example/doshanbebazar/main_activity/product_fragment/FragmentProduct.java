package com.example.doshanbebazar.main_activity.product_fragment;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.doshanbebazar.R;
import com.example.doshanbebazar.models.Product;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentProduct#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentProduct extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    Product product ;


    private GoogleMap mMap;

    SupportMapFragment mapFragment ;

    TextView tv_name, tv_price, tv_des ;
    ImageView iv_product;

    String pic_base_address="http://poushka.com/shop/public/images/product/";



    public FragmentProduct() {
        // Required empty public constructor
    }



    public static FragmentProduct newInstance(Product product) {
        FragmentProduct fragment = new FragmentProduct();
        Bundle args = new Bundle();
        args.putParcelable("product", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
          product=getArguments().getParcelable("product");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_product, container, false);
        tv_name=view.findViewById(R.id.tv_name_product);
        tv_price=view.findViewById(R.id.tv_price_product);
        tv_des=view.findViewById(R.id.tv_description_product);
         iv_product=view.findViewById(R.id.iv_top_product);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
         mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                Log.e("tttttt",product.getLat() + " : " + product.getLang());
                mMap=googleMap;
                        LatLng latLng = new LatLng(product.getLat(),product.getLang());

        mMap.addMarker(new MarkerOptions().position(latLng));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(karaj));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,12));
            }
        });
        tv_name.setText(product.getName());
        tv_des.setText(product.getDescription());
        tv_price.setText(String.valueOf(product.getPrice()));
        Glide.with(requireContext()).load(pic_base_address+product.getPic()).into(iv_product);

    }

//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//        mMap=googleMap;
//
//        Log.e("testmaplocation", product.getLat() + " : " + product.getLang());
//
//
//        LatLng latLng = new LatLng(product.getLat(),product.getLang());
//
//        mMap.addMarker(new MarkerOptions().position(latLng));
//        //mMap.moveCamera(CameraUpdateFactory.newLatLng(karaj));
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,12));
//    }
}