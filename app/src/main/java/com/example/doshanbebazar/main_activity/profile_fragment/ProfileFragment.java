package com.example.doshanbebazar.main_activity.profile_fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doshanbebazar.R;
import com.example.doshanbebazar.api.ApiInterface;
import com.example.doshanbebazar.api.MyRetrofit;
import com.example.doshanbebazar.models.LoginRequest;
import com.example.doshanbebazar.models.RegisterResponse;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {



    TextView tv_register;
    EditText et_email,et_password;

    Button btn_login ;

    ApiInterface apiInterface;

    ProgressBar progressBar ;

    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        tv_register=view.findViewById(R.id.tv_register);
        et_email=view.findViewById(R.id.et_email_login);
        et_password=view.findViewById(R.id.et_password_login);
        btn_login=view.findViewById(R.id.btn_login);
         progressBar=view.findViewById(R.id.progress_login);

        return view;
    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        apiInterface = MyRetrofit.getRetrofit().create(ApiInterface.class);

        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.container_profile,new RegisterFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = et_email.getText().toString();
                String password = et_password.getText().toString();
                if (email.isEmpty()) {
                    Toast.makeText(requireContext(),"لطفا ایمل خود را وارد نمایید",Toast.LENGTH_LONG).show();
                }else if (password.isEmpty())  {
                    Toast.makeText(requireContext(),"لطفا رمز عبور خود را وارد نمایید",Toast.LENGTH_LONG).show();
                }else {
                    LoginRequest loginRequest = new LoginRequest(email,password);
                    progressBar.setVisibility(View.VISIBLE);
                    Call<RegisterResponse> call = apiInterface.login(loginRequest);
                    call.enqueue(new Callback<RegisterResponse>() {
                        @Override
                        public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                            progressBar.setVisibility(View.GONE);
                               if (response.isSuccessful()) {
                                   Toast.makeText(requireContext(),"خوش امدید",Toast.LENGTH_LONG).show();

                               }else {
                                   JSONObject jObjError = null;
                                   try {
                                       jObjError = new JSONObject(response.errorBody().string());
                                       Toast.makeText(getContext(), jObjError.getString("message"), Toast.LENGTH_LONG).show();
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   } catch (IOException e) {
                                       e.printStackTrace();
                                   }
                               }
                        }




                        @Override
                        public void onFailure(Call<RegisterResponse> call, Throwable t) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getContext(), "عدم دسترسی به اینترنت", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
    }
}
