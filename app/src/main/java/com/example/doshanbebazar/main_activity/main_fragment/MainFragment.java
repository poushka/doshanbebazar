package com.example.doshanbebazar.main_activity.main_fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.doshanbebazar.R;
import com.example.doshanbebazar.api.ApiInterface;
import com.example.doshanbebazar.api.MyRetrofit;
import com.example.doshanbebazar.main_activity.product_fragment.FragmentProduct;
import com.example.doshanbebazar.models.DeleteResponse;
import com.example.doshanbebazar.models.Product;
import com.example.doshanbebazar.models.ProductResponse;

import java.lang.reflect.Array;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainFragment extends Fragment implements AdapterProduct.MahsulListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


   ApiInterface apiInterface;

   ProductResponse productResponse;

   ArrayList<Product> products =new ArrayList<>();
   AdapterProduct adapter ;

   RecyclerView rv_product ;

   ProgressBar progressBar ;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface= MyRetrofit.getRetrofit().create(ApiInterface.class);

        adapter=new AdapterProduct(requireContext(),products,this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_main, container, false);
        rv_product=view.findViewById(R.id.rv_product);
        progressBar=view.findViewById(R.id.progrss_product);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        rv_product.setAdapter(adapter);
        apiInterface=MyRetrofit.getRetrofit().create(ApiInterface.class);

        Call<ProductResponse> call = apiInterface.getProducts();
        call.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if (response.isSuccessful()) {
                    productResponse=response.body();
                    products.addAll(productResponse.getProducts());
                    adapter.notifyDataSetChanged();
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), productResponse.getProducts().get(0).getName(), Toast.LENGTH_SHORT).show();


                }else {

                }

            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                Log.e("tts",t.getMessage());
                Toast.makeText(getActivity(), "دسترسی به اینترنت یافت نشد", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void mahsulClicked(Integer position) {

        getChildFragmentManager().beginTransaction().replace(R.id.container_home,
                FragmentProduct.newInstance(products.get(position))).addToBackStack(null).commit();


      //  Toast.makeText(requireContext(),products.get(position).getName(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void deleteCliced(final Integer position) {
        final Integer product_id = products.get(position).getProduct_id();

        AlertDialog.Builder alert = new AlertDialog.Builder(requireContext());

        alert.setTitle("ایا از حذف ایم محصول اطمینان دارید");
        alert.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Call<DeleteResponse> deleteResponseCall = apiInterface.deleteProduct(product_id);
                deleteResponseCall.enqueue(new Callback<DeleteResponse>() {
                    @Override
                    public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {

                        if (response.isSuccessful()){
                            products.remove(position);
                            adapter.notifyItemRemoved(position);
                        }else {
                            Toast.makeText(getActivity(), "خطا در حذف", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<DeleteResponse> call, Throwable t) {
                        Toast.makeText(getActivity(), "دسترسی به اینترنت یافت نشد", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        alert.setNegativeButton("no", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();



    }
}


