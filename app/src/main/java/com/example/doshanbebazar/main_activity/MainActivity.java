package com.example.doshanbebazar.main_activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.doshanbebazar.R;
import com.example.doshanbebazar.main_activity.category_fragment.CategoryFragment;
import com.example.doshanbebazar.main_activity.main_fragment.MainFragment;
import com.example.doshanbebazar.main_activity.new_product.NewProductFragment;
import com.example.doshanbebazar.main_activity.offer_fragment.OfferFragment;
import com.example.doshanbebazar.main_activity.profile_fragment.ProfileFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {


    BottomNavigationView bnv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bnv=findViewById(R.id.navigation_main);
        openFragment(new MainFragment());
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId()==R.id.bnv_home) {
                    openFragment(new MainFragment());
                 }else if (item.getItemId()==R.id.bnv_category) {
                    openFragment(new CategoryFragment());
                }else if (item.getItemId()==R.id.bnv_offer) {
                    openFragment(new NewProductFragment());
                }else if (item.getItemId()==R.id.bnv_profile) {
                    openFragment(new ProfileFragment());
                }
                return true;
            }
        });


    }


    public void openFragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container_main_navigation,fragment);
        fragmentTransaction.commit();
    }


}
