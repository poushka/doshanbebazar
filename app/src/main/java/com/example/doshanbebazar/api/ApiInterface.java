package com.example.doshanbebazar.api;

import com.example.doshanbebazar.models.CategoryResponse;
import com.example.doshanbebazar.models.DeleteResponse;
import com.example.doshanbebazar.models.LoginRequest;
import com.example.doshanbebazar.models.NewProductResponse;
import com.example.doshanbebazar.models.ProductResponse;
import com.example.doshanbebazar.models.RegisterRequest;
import com.example.doshanbebazar.models.RegisterResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiInterface {


    @POST("register")
    public Call<RegisterResponse> register(@Body RegisterRequest registerRequest);

    @POST("login")
    public Call<RegisterResponse> login(@Body LoginRequest loginRequest);

    @GET("getProducts")
    public Call<ProductResponse> getProducts();

    @GET("getCategories")
    public Call<CategoryResponse> getCategories();


   @GET("getProductByCatId/{cat_id}")
    public Call<ProductResponse> getProductsByCat(@Path("cat_id") int cat_id);


    @Multipart
    @POST("makeProduct/{cat_id}")
    public Call<NewProductResponse> makeProduct(@Path("cat_id") int cat_id,@Part MultipartBody.Part pic_part ,
                                                @Part("name") RequestBody name,@Part("price") RequestBody price,
                                                @Part("lat") RequestBody lat , @Part("lang") RequestBody lang ,
                                                @Part("description") RequestBody description
                                                );




    @DELETE("deleteProduct/{product}")
    public Call<DeleteResponse> deleteProduct(@Path("product")  Integer product_id);


}
