package com.example.doshanbebazar.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyRetrofit {

    public static Retrofit retrofit = null ;

    public static Retrofit getRetrofit () {
        if (retrofit!=null) {
           return  retrofit;
        }else {
            retrofit = new Retrofit.Builder().baseUrl("http://poushka.com/shop/public/api/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            return retrofit;

        }

    }


}
