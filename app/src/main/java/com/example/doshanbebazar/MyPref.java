package com.example.doshanbebazar;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.contentcapture.ContentCaptureCondition;

public class MyPref {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    public MyPref(Context context) {
        sharedPreferences=context.getSharedPreferences("Setting",Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
    }
    public void setIntro(boolean state) {
        editor.putBoolean("intro",state);
        editor.apply();
    }


    public boolean getIntro() {
        return   sharedPreferences.getBoolean("intro",false);

    }

}



